package com.bootcamp.employee.dtomodels;

import java.math.BigDecimal;

public class PenempatanDTO {
	private Long idPenempatan;
	private String kotaPenempatan;
	private BigDecimal umkPenempatan;
	
	public PenempatanDTO() {}
	
	public PenempatanDTO(Long idPenempatan, String kotaPenempatan, BigDecimal umkPenempatan) {
		super();
		this.idPenempatan = idPenempatan;
		this.kotaPenempatan = kotaPenempatan;
		this.umkPenempatan = umkPenempatan;
	}
	public Long getIdPenempatan() {
		return idPenempatan;
	}
	public void setIdPenempatan(Long idPenempatan) {
		this.idPenempatan = idPenempatan;
	}
	public String getKotaPenempatan() {
		return kotaPenempatan;
	}
	public void setKotaPenempatan(String kotaPenempatan) {
		this.kotaPenempatan = kotaPenempatan;
	}
	public BigDecimal getUmkPenempatan() {
		return umkPenempatan;
	}
	public void setUmkPenempatan(BigDecimal umkPenempatan) {
		this.umkPenempatan = umkPenempatan;
	}
}
