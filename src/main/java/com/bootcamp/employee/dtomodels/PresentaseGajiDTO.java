package com.bootcamp.employee.dtomodels;

import java.math.BigDecimal;

public class PresentaseGajiDTO {
	private Long idPresentaseGaji;
	private PosisiDTO posisi;
	private TingkatanDTO tingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;
	
	public PresentaseGajiDTO() {}
	
	public PresentaseGajiDTO(Long idPresentaseGaji, PosisiDTO posisi, TingkatanDTO tingkatan, BigDecimal besaranGaji,
			Integer masaKerja) {
		super();
		this.idPresentaseGaji = idPresentaseGaji;
		this.posisi = posisi;
		this.tingkatan = tingkatan;
		this.besaranGaji = besaranGaji;
		this.masaKerja = masaKerja;
	}
	public Long getIdPresentaseGaji() {
		return idPresentaseGaji;
	}
	public void setIdPresentaseGaji(Long idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}
	public PosisiDTO getPosisi() {
		return posisi;
	}
	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}
	public TingkatanDTO getTingkatan() {
		return tingkatan;
	}
	public void setTingkatan(TingkatanDTO tingkatan) {
		this.tingkatan = tingkatan;
	}
	public BigDecimal getBesaranGaji() {
		return besaranGaji;
	}
	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
}
