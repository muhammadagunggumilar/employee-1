package com.bootcamp.employee.dtomodels;

public class KategoriKemampuanDTO {
	private Long idKategori;
	private String namaKategori;
	
	public KategoriKemampuanDTO() {}
	
	public KategoriKemampuanDTO(Long idKategori, String namaKategori) {
		super();
		this.idKategori = idKategori;
		this.namaKategori = namaKategori;
	}
	public Long getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(Long idKategori) {
		this.idKategori = idKategori;
	}
	public String getNamaKategori() {
		return namaKategori;
	}
	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
}
