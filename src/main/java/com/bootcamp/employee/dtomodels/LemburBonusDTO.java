package com.bootcamp.employee.dtomodels;

import java.util.Date;

public class LemburBonusDTO {
	private Long idLemburBonus;
	private KaryawanDTO karyawan;
	private Date tanggalLemburBonus;
	private int lamaLembur;
	private int variableBonus;
	
	public LemburBonusDTO() {}
	
	public LemburBonusDTO(Long idLemburBonus, KaryawanDTO karyawan, Date tanggalLemburBonus, int lamaLembur,
			int variableBonus) {
		super();
		this.idLemburBonus = idLemburBonus;
		this.karyawan = karyawan;
		this.tanggalLemburBonus = tanggalLemburBonus;
		this.lamaLembur = lamaLembur;
		this.variableBonus = variableBonus;
	}

	public Long getIdLemburBonus() {
		return idLemburBonus;
	}

	public void setIdLemburBonus(Long idLemburBonus) {
		this.idLemburBonus = idLemburBonus;
	}

	public KaryawanDTO getKaryawan() {
		return karyawan;
	}

	public void setKaryawan(KaryawanDTO karyawanDTO) {
		this.karyawan = karyawanDTO;
	}

	public Date getTanggalLemburBonus() {
		return tanggalLemburBonus;
	}

	public void setTanggalLemburBonus(Date tanggalLemburBonus) {
		this.tanggalLemburBonus = tanggalLemburBonus;
	}

	public int getLamaLembur() {
		return lamaLembur;
	}

	public void setLamaLembur(int lamaLembur) {
		this.lamaLembur = lamaLembur;
	}

	public int getVariableBonus() {
		return variableBonus;
	}

	public void setVariableBonus(int variableBonus) {
		this.variableBonus = variableBonus;
	}
}
