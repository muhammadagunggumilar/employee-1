package com.bootcamp.employee.interfaces;

public interface BonusInterface {
	int MAXBONUSPROGRAMMER = 500000;
	int MAXBONUSTESTER = 500000;
	int MAXBONUSTECHNICALWRITTER = 500000;
}
