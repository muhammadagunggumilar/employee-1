package com.bootcamp.employee.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="lembur_bonus", schema = "public")
public class LemburBonus implements java.io.Serializable{
	private Long idLemburBonus;
	private Karyawan karyawan;
	private Date tanggalLemburBonus;
	private int lamaLembur;
	private int variableBonus;
	
	public LemburBonus() {}
	
	public LemburBonus(Long idLemburBonus, Karyawan karyawan, Date tanggalLemburBonus, int lamaLembur,
			int variableBonus) {
		super();
		this.idLemburBonus = idLemburBonus;
		this.karyawan = karyawan;
		this.tanggalLemburBonus = tanggalLemburBonus;
		this.lamaLembur = lamaLembur;
		this.variableBonus = variableBonus;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_lembur_bonus_id_lembur_bonus_seq")
	@SequenceGenerator(name="generator_lembur_bonus_id_lembur_bonus_seq", sequenceName="lembur_bonus_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_lembur_bonus", unique = true, nullable = false)
	public Long getIdLemburBonus() {
		return idLemburBonus;
	}
	public void setIdLemburBonus(Long idLemburBonus) {
		this.idLemburBonus = idLemburBonus;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_karyawan")
	public Karyawan getKaryawan() {
		return this.karyawan;
	}
	public void setKaryawan(Karyawan karyawan) {
		this.karyawan = karyawan;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "tanggal_lembur_bonus", nullable = false, length = 13)
	public Date getTanggalLemburBonus() {
		return this.tanggalLemburBonus;
	}
	public void setTanggalLemburBonus(Date tanggalLemburBonus) {
		this.tanggalLemburBonus = tanggalLemburBonus;
	}
	
	@Column(name = "lama_lembur", nullable = false)
	public int getLamaLembur() {
		return this.lamaLembur;
	}
	public void setLamaLembur(int lamaLembur) {
		this.lamaLembur = lamaLembur;
	}
	
	@Column(name = "variable_bonus", nullable = false)
	public int getVariableBonus() {
		return this.variableBonus;
	}
	public void setVariableBonus(int variableBonus) {
		this.variableBonus = variableBonus;
	}
	
}
