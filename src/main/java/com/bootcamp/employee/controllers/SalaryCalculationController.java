package com.bootcamp.employee.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.KaryawanDTO;
import com.bootcamp.employee.dtomodels.LemburBonusDTO;
import com.bootcamp.employee.dtomodels.ParameterDTO;
import com.bootcamp.employee.dtomodels.PendapatanDTO;
import com.bootcamp.employee.dtomodels.PresentaseGajiDTO;
import com.bootcamp.employee.dtomodels.TunjanganPegawaiDTO;
import com.bootcamp.employee.interfaces.BonusInterface;
import com.bootcamp.employee.interfaces.KotaPenempatanInterface;
import com.bootcamp.employee.interfaces.PPHInterface;
import com.bootcamp.employee.interfaces.PosisiInterface;
import com.bootcamp.employee.models.Karyawan;
import com.bootcamp.employee.models.LemburBonus;
import com.bootcamp.employee.models.Parameter;
import com.bootcamp.employee.models.Pendapatan;
import com.bootcamp.employee.models.PresentaseGaji;
import com.bootcamp.employee.models.TunjanganPegawai;
import com.bootcamp.employee.repositories.KaryawanRepository;
import com.bootcamp.employee.repositories.LemburBonusRepository;
import com.bootcamp.employee.repositories.ParameterRepository;
import com.bootcamp.employee.repositories.PendapatanRepository;
import com.bootcamp.employee.repositories.PenempatanRepository;
import com.bootcamp.employee.repositories.PosisiRepository;
import com.bootcamp.employee.repositories.PresentaseGajiRepository;
import com.bootcamp.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api/salaryCalculation")
public class SalaryCalculationController implements KotaPenempatanInterface, PPHInterface, PosisiInterface, BonusInterface{
	@Autowired
	PendapatanRepository pendapatanRepository;
	
	@Autowired
	KaryawanRepository karyawanRepository;
	
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@Autowired
	PenempatanRepository penempatanRepository;
	
	@Autowired
	LemburBonusRepository lemburBonusRepository;
	
	@Autowired
	PosisiRepository posisiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//gajipokok
	@PutMapping("/setAllSalary/{date}")
	public Map<String, Object> setSalary(@PathVariable(value="date") String date){
		DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		LocalDate localDate = LocalDate.parse(date, formater);
		Date dateTime = java.sql.Date.valueOf(localDate);
		
		Map<String, Object> result = new HashMap<>();
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		
		//mapping karayawan to karyawan dto
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(karyawanDTO);
		}
				
		//count salary
		for(KaryawanDTO item : listKaryawanDTO) {
			BigDecimal 	gajiPokok = null, tunjanganKeluarga = null,
						tunjanganPegawai = null, tunjanganTransport = null,
						gajiKotor = null, pph = null, bpjs = null, uangBonus = null,
						gajiBersih = null, takeHomePay = null, uangLembur = null;
			int lamaLembur = 0,  variableBonus = 0;
			
			//set salary
			gajiPokok = countGajiPokok(item);
			tunjanganKeluarga = countTunjanganKeluarga(gajiPokok, item);
			tunjanganPegawai = countTunjanganPegawai(item);
			tunjanganTransport = countTunjanganTransport(item);
			gajiKotor = countGajiKotor(gajiPokok, tunjanganKeluarga, tunjanganPegawai, tunjanganTransport);
			pph = countPPH(gajiKotor);
			bpjs = countBPJS(gajiPokok);
			gajiBersih = countGajiBersih(gajiKotor, pph, bpjs);	
			lamaLembur = countLamaLembur(item, dateTime);
			uangLembur = countUangLembur(gajiBersih, lamaLembur);
			variableBonus = countVariableBonus(item, dateTime);
			uangBonus = countUangBonus(item, variableBonus);
			takeHomePay = countTakeHomePay(gajiBersih, uangLembur, uangBonus);
			
			//mencocokan data karyawan dengan table pendapatan
			Long idKaryawan = item.getIdKaryawan();
			List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
			List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
			
			//mapping pendapatan to pendapatan dto
			for(Pendapatan list : listPendapatan) {
				PendapatanDTO pendapatanDTO = modelMapper.map(list, PendapatanDTO.class);
				listPendapatanDTO.add(pendapatanDTO);
			}
			
			Boolean add = false;
			Boolean update = false;
			Boolean notFound = true;
			
			for(PendapatanDTO list : listPendapatanDTO) {
				//cari id
				Long idKaryawanPendapatan = list.getKaryawan().getIdKaryawan();
				if(idKaryawan == idKaryawanPendapatan) {
					//cari tanggal
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(dateTime);
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH);
					
					Date dateGaji = list.getTanggalGaji();
					Calendar calendarGaji = Calendar.getInstance();
					calendarGaji.setTime(dateGaji);
					int tahunGaji = calendarGaji.get(Calendar.YEAR);
					int bulanGaji = calendarGaji.get(Calendar.MONTH);
					
					if(year == tahunGaji) {
						if(month == bulanGaji) {
							update = true;
							notFound = false;
						}
						else if((month - bulanGaji) >= 1) {
							add = true;
							notFound = false;
						}
					}
				}
			}
			
			//jika belum ada 
			if(notFound || add) {
				Pendapatan pendapatan = new Pendapatan();
				Karyawan karyawan = modelMapper.map(item, Karyawan.class);
				
				pendapatan.setKaryawan(karyawan);
				pendapatan.setTanggalGaji(dateTime);
				pendapatan.setGajiPokok(gajiPokok);
				pendapatan.setTunjanganKeluarga(tunjanganKeluarga);
				pendapatan.setTunjanganPegawai(tunjanganPegawai);
				pendapatan.setTunjanganTransport(tunjanganTransport);
				pendapatan.setGajiKotor(gajiKotor);
				pendapatan.setPphPerbulan(pph);
				pendapatan.setBpjs(bpjs);
				pendapatan.setGajiBersih(gajiBersih);
				pendapatan.setLamaLembur(lamaLembur);
				pendapatan.setUangLembur(uangLembur);
				pendapatan.setVariableBonus(variableBonus);
				pendapatan.setUangBonus(uangBonus);
				pendapatan.setTakeHomePay(takeHomePay);
				
				pendapatanRepository.save(pendapatan);
			}
			
			if(update) {
				List<Pendapatan> listPendapatanKaryawan = pendapatanRepository.findAll();
				List<PendapatanDTO> listPendapatanKaryawanDTO = new ArrayList<PendapatanDTO>();
				Long idPendapatan = null;
				//mapping
				for(Pendapatan itemPendapatanKaryawan : listPendapatanKaryawan) {
					PendapatanDTO pendapatanDTO = modelMapper.map(itemPendapatanKaryawan, PendapatanDTO.class);
					listPendapatanKaryawanDTO.add(pendapatanDTO);
				}
				
				for(PendapatanDTO itemPendapatan : listPendapatanKaryawanDTO) {
					Long idKaryawanPendapatan = itemPendapatan.getKaryawan().getIdKaryawan();
					if(idKaryawanPendapatan == idKaryawan) {
						idPendapatan = itemPendapatan.getIdPendapatan();
					}
				}
				
				Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
				Karyawan karyawan = modelMapper.map(item, Karyawan.class);
				
				pendapatan.setIdPendapatan(idPendapatan);
				pendapatan.setKaryawan(karyawan);
				pendapatan.setTanggalGaji(dateTime);
				pendapatan.setGajiPokok(gajiPokok);
				pendapatan.setTunjanganKeluarga(tunjanganKeluarga);
				pendapatan.setTunjanganPegawai(tunjanganPegawai);
				pendapatan.setTunjanganTransport(tunjanganTransport);
				pendapatan.setGajiKotor(gajiKotor);
				pendapatan.setPphPerbulan(pph);
				pendapatan.setBpjs(bpjs);
				pendapatan.setGajiBersih(gajiBersih);
				pendapatan.setLamaLembur(lamaLembur);
				pendapatan.setUangLembur(uangLembur);
				pendapatan.setVariableBonus(variableBonus);
				pendapatan.setUangBonus(uangBonus);
				pendapatan.setTakeHomePay(takeHomePay);

				pendapatanRepository.save(pendapatan);
			}
		}
		
		result.put("Status", 200);
		result.put("Message", "Set Salary Calculation SUCCESSFULL");
		
		return result;
	}
	
	//count gaji pokok
	public BigDecimal countGajiPokok(KaryawanDTO karyawanDTO) {
		BigDecimal result = null, gaji = null;
		Long posisiKaryawan = karyawanDTO.getPosisi().getIdPosisi();
		Long tingkatanKaryawan = karyawanDTO.getTingkatan().getIdTingkatan();
		int masaKerjaKaryawan = karyawanDTO.getMasaKerja();
		BigDecimal umk = karyawanDTO.getPenempatan().getUmkPenempatan();
		
		int masaKerja = 0, minMasaKerja = 0;
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<>();
		List<PresentaseGajiDTO> listMasaKerja = new ArrayList<>();
		
		//mapping
		for(PresentaseGaji list : listPresentaseGaji) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(list, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		
		//membuat list untuk tingkatan dan posisi
		for(PresentaseGajiDTO item : listPresentaseGajiDTO) {
			Long tingkatan = item.getTingkatan().getIdTingkatan();
			Long posisi = item.getPosisi().getIdPosisi();
			if(tingkatan == tingkatanKaryawan && posisi == posisiKaryawan){
				listMasaKerja.add(item);
			}
		}
		
		//mencari nilai max
		PresentaseGajiDTO presentaseGajiDTO = Collections.max(listMasaKerja, Comparator.comparing(l -> l.getMasaKerja()));
		masaKerja = presentaseGajiDTO.getMasaKerja();
		
		//mencari besaran gaji
		for(PresentaseGajiDTO item : listMasaKerja) {
			masaKerja = item.getMasaKerja();				
			if(masaKerjaKaryawan == masaKerja || masaKerjaKaryawan >= minMasaKerja){
				gaji = item.getBesaranGaji();
			}
		}

		result = gaji.multiply(umk);
		
		return result;
	}
	
	//count tunjangankeluarga
	public BigDecimal countTunjanganKeluarga(BigDecimal gajiPokok, KaryawanDTO karyawanDTO) {
		BigDecimal result = null, tunjanganKeluarga = null;
		int statusPernikahan = karyawanDTO.getStatusPernikahan();
		
		if(statusPernikahan==1) {
			List<Parameter> listParameter = parameterRepository.findAll();
			List<ParameterDTO> listParameterDTO = new ArrayList<>();
			
			//mapping
			for(Parameter item : listParameter) {
				ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
				listParameterDTO.add(parameterDTO);
			}
			
			for(ParameterDTO item : listParameterDTO) {
				tunjanganKeluarga = item.getTKeluarga();
			}
		}
		else {
			tunjanganKeluarga = new BigDecimal(0) ;
		}

		result = gajiPokok.multiply(tunjanganKeluarga);
		return result;
	}
	
	//count tunjanganpegawai
	public BigDecimal countTunjanganPegawai(KaryawanDTO karyawanDTO) {
		BigDecimal result = null;
		Long posisiKaryawan = karyawanDTO.getPosisi().getIdPosisi();
		Long tingkatanKaryawan = karyawanDTO.getTingkatan().getIdTingkatan();
		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<>();
		
		//mapping
		for(TunjanganPegawai item : listTunjanganPegawai) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(item, TunjanganPegawaiDTO.class);
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		
		for(TunjanganPegawaiDTO item : listTunjanganPegawaiDTO) {
			Long posisi = item.getPosisi().getIdPosisi();
			Long tingkatan = item.getTingkatan().getIdTingkatan();
			if(posisi==posisiKaryawan && tingkatan == tingkatanKaryawan) {
				result = item.getBesaranTujnaganPegawai();
			}
		}
		
		return result;
	}
	
	//count tunjangantransport
	public BigDecimal countTunjanganTransport(KaryawanDTO karyawanDTO) {
		BigDecimal result = null;
		String penempatan = karyawanDTO.getPenempatan().getKotaPenempatan();
		if(penempatan.equals(JAKARTA)) {
			List<Parameter> listParameter = parameterRepository.findAll();
			List<ParameterDTO> listParameterDTO = new ArrayList<>();
			
			//mapping
			for(Parameter item : listParameter) {
				ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
				listParameterDTO.add(parameterDTO);
			}
			
			for(ParameterDTO item : listParameterDTO) {
				result = item.getTTransport();
			}
		}
		else {
			result = new BigDecimal(0);
		}
		
		return result;
	}
	
	//count gaji kotor
	public BigDecimal countGajiKotor(BigDecimal gajiPokok, BigDecimal tunjanganKeluarga, BigDecimal tunjanganPegawai, BigDecimal tunjanganTransport) {
		BigDecimal result = null;
		result = gajiPokok.add(tunjanganKeluarga.add(tunjanganPegawai.add(tunjanganTransport)));
		return result;
	}
	
	//count PPH
	public BigDecimal countPPH(BigDecimal gajiKotor) {
		BigDecimal result = null;
		BigDecimal pph = new BigDecimal(PERSENTASEPPH);
		
		result = gajiKotor.multiply(pph);
		
		return result;
	}
	
	//count bpjs
	public BigDecimal countBPJS(BigDecimal gajiPokok) {
		BigDecimal result = null, bpjs = null;
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<>();
		
		//mapping
		for(Parameter item : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		
		for(ParameterDTO item : listParameterDTO) {
			bpjs = item.getPBpjs();
		}
		
		result = gajiPokok.multiply(bpjs);
		return result;
	}
	
	//count GajiBersih
	public BigDecimal countGajiBersih(BigDecimal gajiKotor, BigDecimal pph, BigDecimal bpjs) {
		BigDecimal result = null;
		
		result = gajiKotor.subtract(pph.add(bpjs));
		
		return result;
	}
	
	//count lama lembur
	public int countLamaLembur(KaryawanDTO karyawanDTO, Date date) {
		int result = 0;
		Long idKaryawan = karyawanDTO.getIdKaryawan();
		Calendar calendarDTO = Calendar.getInstance();
		calendarDTO.setTime(date);
		int tahunLemburDTO = calendarDTO.get(Calendar.YEAR);
		int bulanLemburDTO = calendarDTO.get(Calendar.MONTH);
		
		List<LemburBonus> listLemburBonus = lemburBonusRepository.findAll();
		List<LemburBonusDTO> listLemburBonusDTO = new ArrayList<>();
		
		//mapping
		for(LemburBonus item : listLemburBonus) {
			LemburBonusDTO lemburBonusDTO = modelMapper.map(item, LemburBonusDTO.class);
			listLemburBonusDTO.add(lemburBonusDTO);
		}
		
		//cari lembur
		for(LemburBonusDTO item : listLemburBonusDTO) {
			Long idKaryawanLembur = item.getKaryawan().getIdKaryawan();
			if(idKaryawan == idKaryawanLembur) {
				Date dateLembur = item.getTanggalLemburBonus();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateLembur);
				int tahunLembur = calendar.get(Calendar.YEAR);
				int bulanLembur = calendar.get(Calendar.MONTH);
				if(tahunLembur == tahunLemburDTO && bulanLembur == bulanLemburDTO) {
					result = item.getLamaLembur();
				}
			}
		}
		
		return result;
	}
	
	//count uang lembur
	public BigDecimal countUangLembur(BigDecimal gajiBersih, int lamaLembur) {
		BigDecimal result = null, indexLembur = null;
		BigDecimal lembur = new BigDecimal(lamaLembur);
		
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<>();
		
		//mapping
		for(Parameter item : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		
		//cari index lembur
		for(ParameterDTO item : listParameterDTO) {
			indexLembur = item.getLembur();
		}
		
		result = indexLembur.multiply(lembur.multiply(gajiBersih));
		
		return result;
	}
	
	//count variable bonus
	public int countVariableBonus(KaryawanDTO karyawanDTO, Date date) {
		int result = 0;

		Long idKaryawan = karyawanDTO.getIdKaryawan();
		Calendar calendarDTO = Calendar.getInstance();
		calendarDTO.setTime(date);
		int tahunLemburDTO = calendarDTO.get(Calendar.YEAR);
		int bulanLemburDTO = calendarDTO.get(Calendar.MONTH);
		
		List<LemburBonus> listLemburBonus = lemburBonusRepository.findAll();
		List<LemburBonusDTO> listLemburBonusDTO = new ArrayList<>();
		
		//mapping
		for(LemburBonus item : listLemburBonus) {
			LemburBonusDTO lemburBonusDTO = modelMapper.map(item, LemburBonusDTO.class);
			listLemburBonusDTO.add(lemburBonusDTO);
		}

		//cari bonus
		for(LemburBonusDTO item : listLemburBonusDTO) {
			Long idKaryawanLembur = item.getKaryawan().getIdKaryawan();
			if(idKaryawan == idKaryawanLembur) {
				Date dateLembur = item.getTanggalLemburBonus();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateLembur);
				int tahunLembur = calendar.get(Calendar.YEAR);
				int bulanLembur = calendar.get(Calendar.MONTH);
				if(tahunLembur == tahunLemburDTO && bulanLembur == bulanLemburDTO) {
					result = item.getVariableBonus();
				}
			}
		}
		
		return result;		
	}
	
	//count uang bonus
	public BigDecimal countUangBonus(KaryawanDTO karyawanDTO, int variableBonus) {
		BigDecimal result = null, bonus = null, uangBonus = null, maxBonus = null;
		double persentase = 0;
		int batasBonus = 0;
		
		bonus = countBonus(karyawanDTO);
		batasBonus = countBatasBonus(karyawanDTO);
		
		if(batasBonus==0 || variableBonus == 0) {
			uangBonus = new BigDecimal(0);
		}
		else {
			persentase = (double)(variableBonus/batasBonus);
			BigDecimal persen = new BigDecimal(persentase);
			uangBonus = bonus.multiply(persen);
		} 
		
		maxBonus = countMaxBonus(karyawanDTO);
		
		if(uangBonus.compareTo(maxBonus) > 0) {
			result = maxBonus;
		}
		else {
			result = uangBonus;
		}
		
		return result;
	}
	
	//count take home pay
	public BigDecimal countTakeHomePay(BigDecimal gajiBersih, BigDecimal uangLembur, BigDecimal uangBonus) {
		BigDecimal result = null;
		
		result = gajiBersih.add(uangLembur.add(uangBonus));
		
		return result;
	}
	
	//count bonus
	public BigDecimal countBonus(KaryawanDTO karyawanDTO) {
		BigDecimal result = null;
		
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<>();
		
		//mapping
		for(Parameter item : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		
		String posisiKaryawan = karyawanDTO.getPosisi().getNamaPosisi();
		if(posisiKaryawan.equalsIgnoreCase(PROGRAMMER)){
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBonusPg();
			}
		}
		else if(posisiKaryawan.equalsIgnoreCase(TESTER)) {
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBonusTs();
			}
		}
		else if(posisiKaryawan.equalsIgnoreCase(TECHNICALWRITTER)) {
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBonusTw();
			}
		}
		else {
			result = new BigDecimal(0);
		}
		
		return result;
	}
	
	//count batas bonus
	public int countBatasBonus(KaryawanDTO karyawanDTO) {
		int result = 0;
		
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<>();
		
		//mapping
		for(Parameter item : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(item, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		
		String posisiKaryawan = karyawanDTO.getPosisi().getNamaPosisi();
		if(posisiKaryawan.equalsIgnoreCase(PROGRAMMER)){
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBatasanBonusPg();
			}
		}
		else if(posisiKaryawan.equalsIgnoreCase(TESTER)) {
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBatasanBonusTs();
			}
		}
		else if(posisiKaryawan.equalsIgnoreCase(TECHNICALWRITTER)) {
			for(ParameterDTO item : listParameterDTO) {
				result = item.getBatasanBonusTw();
			}
		}
		return result;
	}
	
	//count max bonus
	public BigDecimal countMaxBonus(KaryawanDTO karyawanDTO) {
		BigDecimal result = null;
		int maxBonus = 0;
		
		String posisi = karyawanDTO.getPosisi().getNamaPosisi();
		if(posisi.equalsIgnoreCase(PROGRAMMER)) {
			maxBonus = MAXBONUSPROGRAMMER;
		}
		else if(posisi.equalsIgnoreCase(TESTER)) {
			maxBonus = MAXBONUSTESTER;
		}
		else if(posisi.equalsIgnoreCase(TECHNICALWRITTER)) {
			maxBonus = MAXBONUSTECHNICALWRITTER;
		}
		
		result = new BigDecimal(maxBonus);
		return result;
	}
}
