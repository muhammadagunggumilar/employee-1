package com.bootcamp.employee.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.AgamaDTO;
import com.bootcamp.employee.models.Agama;
import com.bootcamp.employee.repositories.AgamaRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;

@RestController
@RequestMapping("/api/agama")
public class AgamaController {
	@Autowired
	AgamaRepository agamaRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createAgama(@Valid @RequestBody AgamaDTO agamaDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = modelMapper.map(agamaDTO, Agama.class);
		agamaRepository.save(agama);
		
		result.put("Status", 200);
		result.put("Message", "Create Agama SUCCESSFULL");
		result.put("Data", agamaDTO);
		
		return result;		
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getAgama(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Agama> listAgama = agamaRepository.findAll();
		List<AgamaDTO> listAgamaDTO = new ArrayList<>();
		
		for(Agama agama : listAgama) {
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			listAgamaDTO.add(agamaDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read Agama SUCCESSFULL");
		result.put("Data", listAgamaDTO);
		
		return result;	
	}
	
	//readbyid
	@GetMapping("/read/{idAgama}")
	public Map<String, Object> getAgamaById(@PathVariable(value="idAgama") Long idAgama){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = agamaRepository.findById(idAgama).get();
		AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read Agama by Id SUCCESSFULL");
		result.put("Data", agamaDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idAgama}")
	public Map<String, Object> updateAgama(@PathVariable(value="idAgama") Long idAgama,
			@Valid @RequestBody AgamaDTO agamaDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = agamaRepository.findById(idAgama).get();
		agama = modelMapper.map(agamaDTO, Agama.class);
		agama.setIdAgama(idAgama);
		agamaRepository.save(agama);
		
		result.put("Status", 200);
		result.put("Message", "Update Agama SUCCESSFULL");
		result.put("Data", agama);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idAgama}")
	public Map<String, Object> deleteAgama(@PathVariable(value="idAgama") Long idAgama){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = agamaRepository.findById(idAgama).get();
		agamaRepository.delete(agama);
		
		result.put("Status", 200);
		result.put("Message", "Delete Agama SUCCESSFULL");
		
		return result;
	}
}
