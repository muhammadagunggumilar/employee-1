package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.PosisiDTO;
import com.bootcamp.employee.models.Posisi;
import com.bootcamp.employee.repositories.PosisiRepository;

@RestController
@RequestMapping("/api/posisi")
public class PosisiController {
	@Autowired
	PosisiRepository posisiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPosisi(@Valid @RequestBody PosisiDTO posisiDTO){
		Map<String, Object> result = new HashMap<>();
		Posisi posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisiRepository.save(posisi);
		
		result.put("Status", 200);
		result.put("Message", "Create Posisi SUCCESSFULL");
		result.put("Data", posisiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPosisi(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Posisi> listPosisi = posisiRepository.findAll();
		List<PosisiDTO> listPosisiDTO = new ArrayList<>();
		
		for(Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(posisiDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Posisi SUCCESSFULL");
		result.put("Data", listPosisiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read/{idPosisi}")
	public Map<String, Object> getPosisiById(@PathVariable(value="idPosisi") Long idPosisi){
		Map<String, Object> result = new HashMap<String, Object>();
		Posisi posisi = posisiRepository.findById(idPosisi).get();
		PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
				
		result.put("Status", 200);
		result.put("Message", "Read Posisi by Id SUCCESSFULL");
		result.put("Data", posisiDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idPosisi}")
	public Map<String, Object> updatePosisi(@PathVariable(value="idPosisi") Long idPosisi,
			@Valid @RequestBody PosisiDTO posisiDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Posisi posisi = posisiRepository.findById(idPosisi).get();
		posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisi.setIdPosisi(idPosisi);
		posisiRepository.save(posisi);
		
		result.put("Status", 200);
		result.put("Message", "Update Posisi SUCCESSFULL");
		result.put("Data", posisiDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idPosisi}")
	public Map<String, Object> deletePosisi(@PathVariable(value="idPosisi") Long idPosisi){
		Map<String, Object> result = new HashMap<String, Object>();
		Posisi posisi = posisiRepository.findById(idPosisi).get();
		posisiRepository.delete(posisi);
		
		result.put("Status", 200);
		result.put("Message", "Delete Posisi SUCCESSFULL");
		
		return result;
	}
}
