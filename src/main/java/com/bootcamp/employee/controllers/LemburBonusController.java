package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.LemburBonusDTO;
import com.bootcamp.employee.models.LemburBonus;
import com.bootcamp.employee.repositories.LemburBonusRepository;

@RestController
@RequestMapping("/api/lemburBonus")
public class LemburBonusController {
	@Autowired
	LemburBonusRepository lemburBonusRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createLemburBonus(@Valid @RequestBody LemburBonusDTO lemburBonusDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		List<LemburBonus> listLemburBonus = lemburBonusRepository.findAll();
		List<LemburBonusDTO> listLemburBonusDTO = new ArrayList<>();
		boolean add = false, notFound = false;
		Long idKaryawanDTO = lemburBonusDTO.getKaryawan().getIdKaryawan();
		Date dateDTO = lemburBonusDTO.getTanggalLemburBonus();
		//extract tanggal dari requestbody
		Calendar calendarDTO = Calendar.getInstance();
		calendarDTO.setTime(dateDTO);
		int tahunDTO = calendarDTO.get(Calendar.YEAR);
		int bulanDTO = calendarDTO.get(Calendar.MONTH);

		//mapping
		for(LemburBonus item : listLemburBonus) {
			LemburBonusDTO data = modelMapper.map(item, LemburBonusDTO.class);
			listLemburBonusDTO.add(data);
		}
		
		//cari data di dalam table lembur bonus dahulu
		for(LemburBonusDTO item : listLemburBonusDTO) {
			Long idKaryawan = item.getKaryawan().getIdKaryawan();
			
			if(idKaryawan == idKaryawanDTO) {
				//extract tanggal dari table lembur bonus
				Date date = item.getTanggalLemburBonus();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				int tahun = calendar.get(Calendar.YEAR);
				int bulan = calendar.get(Calendar.MONTH);
				
				if(tahun == tahunDTO && bulan == bulanDTO) {
					add = false;
					notFound = false;
				}
				else {
					add = true;
				}
			}
			else {
				notFound = true;
			}
		}
		
		if(notFound || add) {
			LemburBonus lemburBonus = modelMapper.map(lemburBonusDTO, LemburBonus.class);
			lemburBonusRepository.save(lemburBonus);
			
			result.put("Status", 200);
			result.put("Message", "Create LemburBonus SUCCESSFULL");
			result.put("Data", lemburBonusDTO);
		}
		else {
			result.put("Status", 500);
			result.put("Message", "Karyawan dengan tanggal yang di input sudah ada dalam database");
		}

		return result;		
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getLemburBonus(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<LemburBonus> listLemburBonus = lemburBonusRepository.findAll();
		List<LemburBonusDTO> listLemburBonusDTO = new ArrayList<>();
		
		for(LemburBonus lemburBonus : listLemburBonus) {
			LemburBonusDTO lemburBonusDTO = modelMapper.map(lemburBonus, LemburBonusDTO.class);
			listLemburBonusDTO.add(lemburBonusDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read LemburBonus SUCCESSFULL");
		result.put("Data", listLemburBonusDTO);
		
		return result;	
	}
	
	//readbyid
	@GetMapping("/read/{idLemburBonus}")
	public Map<String, Object> getLemburBonusById(@PathVariable(value="idLemburBonus") Long idLemburBonus){
		Map<String, Object> result = new HashMap<String, Object>();
		LemburBonus lemburBonus = lemburBonusRepository.findById(idLemburBonus).get();
		LemburBonusDTO lemburBonusDTO = modelMapper.map(lemburBonus, LemburBonusDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read LemburBonus by Id SUCCESSFULL");
		result.put("Data", lemburBonusDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idLemburBonus}")
	public Map<String, Object> updateLemburBonus(@PathVariable(value="idLemburBonus") Long idLemburBonus,
			@Valid @RequestBody LemburBonusDTO lemburBonusDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		LemburBonus lemburBonus = lemburBonusRepository.findById(idLemburBonus).get();
		lemburBonus = modelMapper.map(lemburBonusDTO, LemburBonus.class);
		lemburBonus.setIdLemburBonus(idLemburBonus);
		lemburBonusRepository.save(lemburBonus);
		
		result.put("Status", 200);
		result.put("Message", "Update LemburBonus SUCCESSFULL");
		result.put("Data", lemburBonus);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idLemburBonus}")
	public Map<String, Object> deleteAgama(@PathVariable(value="idLemburBonus") Long idLemburBonus){
		Map<String, Object> result = new HashMap<String, Object>();
		LemburBonus lemburBonus = lemburBonusRepository.findById(idLemburBonus).get();
		lemburBonusRepository.delete(lemburBonus);
		
		result.put("Status", 200);
		result.put("Message", "Delete LemburBonus SUCCESSFULL");
		
		return result;
	}
}
