package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.KaryawanDTO;
import com.bootcamp.employee.models.Karyawan;
import com.bootcamp.employee.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {
	@Autowired
	KaryawanRepository karyawanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createMapping(@Valid @RequestBody KaryawanDTO karyawanDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawanRepository.save(karyawan);
		
		result.put("Status", 200);
		result.put("Message", "Create Karyawan SUCCESSFULL");
		result.put("Data", karyawanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getKaryawan(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList<>();
		
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(karyawanDTO);
		}

		result.put("Status", 200);
		result.put("Message", "Read Karyawan SUCCESSFULL");
		result.put("Data", listKaryawanDTO);
		
		return result;
	}
	
	//readby id
	@GetMapping("/read/{idKaryawan}")
	public Map<String, Object> getKaryawanById(@PathVariable(value="idKaryawan") Long idKaryawan){
		Map<String, Object> result = new HashMap<String, Object>();
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
		KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read Karyawan SUCCESSFULL");
		result.put("Data", karyawanDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idKaryawan}")
	public Map<String, Object> updateKaryawan(@PathVariable(value="idKaryawan") Long idKaryawan,
			@Valid @RequestBody KaryawanDTO karyawanDTO){
		Map<String, Object> result = new HashMap<>();
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
		karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawan.setIdKaryawan(idKaryawan);
		karyawanRepository.save(karyawan);
		
		result.put("Status", 200);
		result.put("Message", "Update Karyawan SUCCESSFULL");
		result.put("Data", karyawanDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idKaryawan}")
	public Map<String, Object> deleteKaryawan(@PathVariable(value="idKaryawan") Long idKaryawan){
		Map<String, Object> result = new HashMap<String, Object>();
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
		karyawanRepository.delete(karyawan);

		result.put("Status", 200);
		result.put("Message", "Delete Karyawan SUCCESSFULL");
		
		return result;
	}
}
