package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.PenempatanDTO;
import com.bootcamp.employee.models.Penempatan;
import com.bootcamp.employee.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api/penempatan")
public class PenempatanController {
	@Autowired
	PenempatanRepository penempatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPenempatan(@Valid @RequestBody PenempatanDTO penempatanDTO){
		Map<String, Object> result = new HashMap<>();
		Penempatan penempatan = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatanRepository.save(penempatan);
		
		result.put("Status", 200);
		result.put("Message", "Create Penempatan SUCCESSFULL");
		result.put("Data", penempatanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPenempatan(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Penempatan> listPenempatan = penempatanRepository.findAll();
		List<PenempatanDTO> listPenempatanDTO = new ArrayList<>();
		
		for(Penempatan penempatan : listPenempatan) {
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			listPenempatanDTO.add(penempatanDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Penempatan SUCCESSFULL");
		result.put("Data", listPenempatanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read/{idPenempatan}")
	public Map<String, Object> getPenempatanById(@PathVariable(value="idPenempatan") Long idPenempatan){
		Map<String, Object> result = new HashMap<String, Object>();
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).get();
		PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
				
		result.put("Status", 200);
		result.put("Message", "Read Penempatan by Id SUCCESSFULL");
		result.put("Data", penempatanDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idPenempatan}")
	public Map<String, Object> updatePenempatan(@PathVariable(value="idPenempatan") Long idPenempatan,
			@Valid @RequestBody PenempatanDTO penempatanDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).get();
		penempatan = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatan.setIdPenempatan(idPenempatan);
		penempatanRepository.save(penempatan);
		
		result.put("Status", 200);
		result.put("Message", "Update Penempatan SUCCESSFULL");
		result.put("Data", penempatanDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idPenempatan}")
	public Map<String, Object> deletePenempatan(@PathVariable(value="idPenempatan") Long idPenempatan){
		Map<String, Object> result = new HashMap<String, Object>();
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).get();
		penempatanRepository.delete(penempatan);
		
		result.put("Status", 200);
		result.put("Message", "Delete Penempatan SUCCESSFULL");
		
		return result;
	}
}
