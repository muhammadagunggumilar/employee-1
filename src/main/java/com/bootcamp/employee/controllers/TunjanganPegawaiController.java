package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.TunjanganPegawaiDTO;
import com.bootcamp.employee.models.TunjanganPegawai;
import com.bootcamp.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api/tunjanganPegawai")
public class TunjanganPegawaiController {
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
		
	//create
	@PostMapping("/create")
	public Map<String, Object> createTunjanganPegawai(@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		Map<String, Object> result = new HashMap<>();
		TunjanganPegawai tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		
		result.put("Status", 200);
		result.put("Message", "Create TunjanganPegawai SUCCESSFULL");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getTunjanganPegawai(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<>();
		
		for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawai) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Tunjangan Pegawai SUCCESSFULL");
		result.put("Data", listTunjanganPegawaiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read/{idTunjanganPegawai}")
	public Map<String, Object> getTunjanganPegawaiById(@PathVariable(value="idTunjanganPegawai") Long idTunjanganPegawai){
		Map<String, Object> result = new HashMap<String, Object>();
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(idTunjanganPegawai).get();
		TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
				
		result.put("Status", 200);
		result.put("Message", "Read TunjanganPegawai by Id SUCCESSFULL");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idTunjanganPegawai}")
	public Map<String, Object> updateTunjanganPegawai(@PathVariable(value="idTunjanganPegawai") Long idTunjanganPegawai,
			@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(idTunjanganPegawai).get();
		tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawai.setIdTunjanganPegawai(idTunjanganPegawai);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		
		result.put("Status", 200);
		result.put("Message", "Update Tunjangan Pegawai SUCCESSFULL");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idTunjanganPegawai}")
	public Map<String, Object> deleteTunjanganPegawai(@PathVariable(value="idTunjanganPegawai") Long idTunjanganPegawai){
		Map<String, Object> result = new HashMap<String, Object>();
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(idTunjanganPegawai).get();
		tunjanganPegawaiRepository.delete(tunjanganPegawai);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tunjangan Pegawai SUCCESSFULL");
		
		return result;
	}
	
}
