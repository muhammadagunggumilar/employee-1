package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.ParameterDTO;
import com.bootcamp.employee.models.Parameter;
import com.bootcamp.employee.repositories.ParameterRepository;

@RestController
@RequestMapping("/api/parameter")
public class ParameterController {
	@Autowired
	ParameterRepository parameterRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createParameter(@Valid @RequestBody ParameterDTO parameterDTO){
		Map<String, Object> result = new HashMap<>();
		Parameter parameter = modelMapper.map(parameterDTO, Parameter.class);
		parameterRepository.save(parameter);
		
		result.put("Status", 200);
		result.put("Message", "Create Parameter SUCCESSFULL");
		result.put("Data", parameterDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getParameter(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<>();
		
		for(Parameter parameter : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(parameter, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Parameter SUCCESSFULL");
		result.put("Data", listParameterDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idParameter}")
	public Map<String, Object> updateParameter(@PathVariable(value="idParameter") Long idParameter,
			@Valid @RequestBody ParameterDTO parameterDTO){
		Map<String, Object> result = new HashMap<>();
		Parameter parameter = parameterRepository.findById(idParameter).get();
		parameter = modelMapper.map(parameterDTO, Parameter.class);
		parameter.setIdParam(idParameter);
		parameterRepository.save(parameter);
		
		result.put("Status", 200);
		result.put("Message", "Update Parameter SUCCESSFULL");
		result.put("Data", parameterDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idParameter}")
	public Map<String, Object> deleteParameter(@PathVariable(value="idParameter") Long idParameter){
		Map<String, Object> result = new HashMap<String, Object>();
		Parameter parameter = parameterRepository.findById(idParameter).get();
		parameterRepository.delete(parameter);
		
		result.put("Status", 200);
		result.put("Message", "Delete Parameter SUCCESSFULL");
		
		return result;
	}
}
