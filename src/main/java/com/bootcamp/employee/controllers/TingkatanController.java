package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.TingkatanDTO;
import com.bootcamp.employee.models.Tingkatan;
import com.bootcamp.employee.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api/tingkatan")
public class TingkatanController {
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
		
	//create
	@PostMapping("/create")
	public Map<String, Object> createTingkatan(@Valid @RequestBody TingkatanDTO tingkatanDTO){
		Map<String, Object> result = new HashMap<>();
		Tingkatan tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatanRepository.save(tingkatan);
		
		result.put("Status", 200);
		result.put("Message", "Create Tingkatan SUCCESSFULL");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getTingkatan(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Tingkatan> listTingkatan = tingkatanRepository.findAll();
		List<TingkatanDTO> listTingkatanDTO = new ArrayList<>();
		
		for(Tingkatan tingkatan : listTingkatan) {
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			listTingkatanDTO.add(tingkatanDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Tingkatan SUCCESSFULL");
		result.put("Data", listTingkatanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read/{idTingkatan}")
	public Map<String, Object> getTingkatanById(@PathVariable(value="idTingkatan") Long idTingkatan){
		Map<String, Object> result = new HashMap<String, Object>();
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
		TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
				
		result.put("Status", 200);
		result.put("Message", "Read Tingkatan by Id SUCCESSFULL");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idTingkatan}")
	public Map<String, Object> updateTingkatan(@PathVariable(value="idTingkatan") Long idTingkatan,
			@Valid @RequestBody TingkatanDTO tingkatanDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
		tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatan.setIdTingkatan(idTingkatan);
		tingkatanRepository.save(tingkatan);
		
		result.put("Status", 200);
		result.put("Message", "Update Tingkatan SUCCESSFULL");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idTingkatan}")
	public Map<String, Object> deleteTingkatan(@PathVariable(value="idTingkatan") Long idTingkatan){
		Map<String, Object> result = new HashMap<String, Object>();
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
		tingkatanRepository.delete(tingkatan);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tingkatan SUCCESSFULL");
		
		return result;
	}
	
}
