package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.PresentaseGajiDTO;
import com.bootcamp.employee.models.PresentaseGaji;
import com.bootcamp.employee.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api/presentaseGaji")
public class PresentaseGajiController {
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPresentaseGaji(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		Map<String, Object> result = new HashMap<>();
		PresentaseGaji presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		presentaseGajiRepository.save(presentaseGaji);
		
		result.put("Status", 200);
		result.put("Message", "Create Presentase Gaji SUCCESSFULL");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPresentaseGaji(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<>();
		
		for(PresentaseGaji presentaseGaji : listPresentaseGaji) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Presentase Gaji SUCCESSFULL");
		result.put("Data", listPresentaseGajiDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read/{idPresentaseGaji}")
	public Map<String, Object> getPresentaseGajiById(@PathVariable(value="idPresentaseGaji") Long idPresentaseGaji){
		Map<String, Object> result = new HashMap<String, Object>();
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
		PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
				
		result.put("Status", 200);
		result.put("Message", "Read Presentase Gaji by Id SUCCESSFULL");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idPresentaseGaji}")
	public Map<String, Object> updatePresentaseGaji(@PathVariable(value="idPresentaseGaji") Long idPresentaseGaji,
			@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
		presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		presentaseGaji.setIdPresentaseGaji(idPresentaseGaji);
		presentaseGajiRepository.save(presentaseGaji);
		
		result.put("Status", 200);
		result.put("Message", "Update Presentase Gaji SUCCESSFULL");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idPresentaseGaji}")
	public Map<String, Object> deletePresentaseGaji(@PathVariable(value="idPresentaseGaji") Long idPresentaseGaji){
		Map<String, Object> result = new HashMap<String, Object>();
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
		presentaseGajiRepository.delete(presentaseGaji);
		
		result.put("Status", 200);
		result.put("Message", "Delete Presentase Gaji SUCCESSFULL");
		
		return result;
	}
}
