package com.bootcamp.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.employee.dtomodels.PendapatanDTO;
import com.bootcamp.employee.models.Pendapatan;
import com.bootcamp.employee.repositories.PendapatanRepository;

@RestController
@RequestMapping("/api/pendapatan")
public class PendapatanController {
	@Autowired
	PendapatanRepository pendapatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPendapatan(@Valid @RequestBody PendapatanDTO pendapatanDTO){
		Map<String, Object> result = new HashMap<>();
		Pendapatan pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
		pendapatanRepository.save(pendapatan);
		
		result.put("Status", 200);
		result.put("Message", "Create Pendapatan SUCCESSFULL");
		result.put("Data", pendapatanDTO);
		
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPendapatan(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList<>();
		
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(pendapatanDTO);
		}	
		
		result.put("Status", 200);
		result.put("Message", "Read Pendapatan SUCCESSFULL");
		result.put("Data", listPendapatanDTO);
		
		return result;
	}
	
	//update
	@PutMapping("/update/{idPendapatan}")
	public Map<String, Object> updatePendapatan(@PathVariable(value="idPendapatan") Long idPendapatan,
			@Valid @RequestBody PendapatanDTO pendapatanDTO){
		Map<String, Object> result = new HashMap<>();
		Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
		pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
		pendapatan.setIdPendapatan(idPendapatan);
		pendapatanRepository.save(pendapatan);
		
		result.put("Status", 200);
		result.put("Message", "Update Pendapatan SUCCESSFULL");
		result.put("Data", pendapatanDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{idPendapatan}")
	public Map<String, Object> deletePendapatan(@PathVariable(value="idPendapatan") Long idPendapatan){
		Map<String, Object> result = new HashMap<String, Object>();
		Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
		pendapatanRepository.delete(pendapatan);
		
		result.put("Status", 200);
		result.put("Message", "Delete Pendapatan SUCCESSFULL");
		
		return result;
	}

}
