package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Kemampuan;

@Repository
public interface KemampuanRepository extends JpaRepository<Kemampuan, Long>{

}
