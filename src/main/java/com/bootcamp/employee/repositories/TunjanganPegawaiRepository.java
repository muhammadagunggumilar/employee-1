package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.TunjanganPegawai;

@Repository
public interface TunjanganPegawaiRepository extends JpaRepository<TunjanganPegawai, Long>{

}
