package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.LemburBonus;

@Repository
public interface LemburBonusRepository extends JpaRepository<LemburBonus, Long>{

}
