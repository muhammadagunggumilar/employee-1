package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Long>{

}
