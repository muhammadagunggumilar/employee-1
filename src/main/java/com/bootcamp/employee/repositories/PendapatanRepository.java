package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Pendapatan;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Long>{

}
