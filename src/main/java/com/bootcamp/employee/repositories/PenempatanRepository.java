package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Penempatan;

@Repository
public interface PenempatanRepository extends JpaRepository<Penempatan, Long>{

}
