package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.PresentaseGaji;

@Repository
public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji, Long>{

}
