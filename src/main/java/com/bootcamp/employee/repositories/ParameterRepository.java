package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Parameter;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long>{

}
