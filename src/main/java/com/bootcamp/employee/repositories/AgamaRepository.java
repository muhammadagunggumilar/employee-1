package com.bootcamp.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.employee.models.Agama;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long>{

}
